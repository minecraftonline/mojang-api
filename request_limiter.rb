require "limiter"

class RequestLimiter < Faraday::Middleware
    def initialize(app, options)
        super

        queue = @queue = Limiter::RateQueue.new(options.delete(:rate), options)
        if app.is_a? Faraday::Request::Retry
            app.instance_eval do
                orig_retry = @options.retry_block
                @options[:retry_block] = lambda do |*args|
                    queue.shift
                    orig_retry.call(*args)
                end
            end
        end
    end

    def on_request(env)
        @queue.shift
    end

    def on_completed(env)
        # If we get a 429, something outside this app got us rate limited.
        # Update queue to proceed at average rate.
        if env[:status] == 429
            puts "Got 429"
            @queue.instance_eval do
                @mutex.synchronize do
                    @ring[@head] = clock.time - @interval + (@rate / @interval)
                end
            end
        end
    end
end
Faraday::Request.register_middleware request_limiter: RequestLimiter

# vim: set et si sts=4 sw=4:
