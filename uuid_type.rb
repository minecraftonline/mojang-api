class UUIDType < ActiveModel::Type::Value
    def serialize(value)
        value.to_s
    end

    def cast_value(value)
        if value.is_a? UUIDTools::UUID
            value
        elsif UUIDTools::UUID_REGEXP.match? value
            UUIDTools::UUID.parse(value)
        else
            UUIDTools::UUID.parse_hexdigest(value)
        end
    end
end
ActiveRecord::Type.register :mc_uuid, UUIDType
