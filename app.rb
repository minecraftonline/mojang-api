#!/usr/bin/ruby
# frozen_sting_literals: true
# vim: set et sts=4 sw=4 si:

require "sinatra"
require "sinatra/activerecord"

require "faraday"
require "faraday_middleware"

require "uuidtools"
require "./request_limiter"

require "./uuid_type"
require "./entry"

configure :development do
    require "better_errors"

    use BetterErrors::Middleware
    BetterErrors.application_root = __dir__
end

configure do
    Faraday.default_connection = Faraday.new do |conn|
        conn.request :request_limiter, rate: 600, interval: 600
        conn.request :retry, retry_statuses: [429], max: 600
        conn.response :json
        conn.response :raise_error
        conn.response :logger, nil, log_level: :debug, headers: false
    end
end

def refresh_uuid(uuid)
    res = Faraday.get("https://api.mojang.com/user/profiles/#{uuid.hexdigest}/names").body

    halt 404 if res.nil?

    # If changedToAt isn't unique for all entries, keep the last
    res = res.reverse.uniq {|r| r['changedToAt']}.reverse

    entries = Entry.where(uuid: uuid).order :start

    entries.each do |entry|
        res.reject! {|r| entry.start.nil? && r['changedToAt'].nil? || Time.at(r['changedToAt'] / 1000r) == entry.start}
    end

    last = entries.last
    new_entries = res.map do |r|
        changed_at = Time.at(r['changedToAt'] / 1000r) unless r['changedToAt'].nil?
        
        if last
            last.end = changed_at
        end
        
        last = entries.new name: r['name'], start: changed_at
    end

    entries.transaction do
        entries.last&.save!
        new_entries.each(&:save!)
    end
end

def populate(username, at = nil)
    at ||= Time.now
    res = Faraday.get("https://api.mojang.com/users/profiles/minecraft/#{username}?at=#{at.to_i}").body

    halt 404 if res.nil?

    refresh_uuid UUIDTools::UUID.parse_hexdigest(res['id'])
end

before do
    headers 'Access-Control-Allow-Origin' => '*'
end

get '/minecraft/:username' do |username|
    at = Time.at(params['at'].to_i) if params['at']
    at ||= Time.now

    entry = Entry.at(at).find_by 'name LIKE ?', username

    if entry.nil?
        logger.info "Populating #{username} at #{at}"
        populate(username)
    elsif entry.stale?
        logger.info "Refreshing uuid #{entry.uuid}"
        refresh_uuid entry.uuid
    end

    entry = Entry.at(at).find_by 'name LIKE ?', username
    halt 421 if entry.nil?

    content_type :json
    {id: entry.uuid.hexdigest, name: entry.name}.to_json
end

get '/:uuid/names' do |uuid|
    uuid = UUIDTools::UUID.parse_hexdigest(uuid)

    entries = Entry.where(uuid: uuid).order :start

    last_entry = entries.last
    refresh_uuid uuid if last_entry.nil? or last_entry.stale?

    content_type :json
    entries.map(&:as_api).to_json
end

get '/' do
    haml :index
end
