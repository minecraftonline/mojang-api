class Entry < ActiveRecord::Base
    validates :name, :uuid, presence: true
    validates :start, :end, uniqueness: { scope: :uuid }
    attribute :uuid, :mc_uuid

    def self.at(time)
        where(start: ..time).or(where(start: nil)).and(where(end: time..).or(where(end: nil)))
    end

    def stale?
        self.end.nil? && updated_at < (Time.now - 5*60) && (start || Time.at(0)) < (Time.now - 30*24*60*60)
    end

    def as_api
        res = {name: name}
        res[:changedToAt] = (1000 * start.to_r).to_i unless start.nil?
        res
    end
end

# vim: set et sts=4 sw=4 si:
