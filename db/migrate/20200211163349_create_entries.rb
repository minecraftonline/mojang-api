class CreateEntries < ActiveRecord::Migration[6.0]
  def change
    create_table :entries do |t|
      t.string :uuid, null: false
      t.string :name, null: false
      t.datetime :start
      t.datetime :end

      t.timestamps
    end
  end
end
